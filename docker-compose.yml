version: '3'

services:
  data-receiver:
    container_name: data-receiver
    build:
      context: data-receiver
    depends_on:
    - data-processing-bus
    - data-reciever-storage
    volumes:
    - /var/logs/:/var/log/
    expose:
    - 8011
    ports:
    - 8011:8011
    environment:
    - SPRING_PROFILES_ACTIVE=${SPRING_PROFILE}
    - SPRING_ACTIVEMQ_BROKER_URL=tcp://data-processing-bus:61616
    - RECEIVER_MINIO_ENDPOINT=http://data-reciever-storage:9000

  data-processor:
    container_name: data-processor
    build:
      context: data-processor
    volumes:
    - /var/logs/:/var/log/
    expose:
    - 8012
    ports:
    - 8012:8012
    depends_on:
    - data-processor-storage
    - data-receiver
    environment:
    - SPRING_PROFILES_ACTIVE=${SPRING_PROFILE}
    - SPRING_ACTIVEMQ_BROKER_URL=tcp://data-processing-bus:61616

  data-analyzer:
    container_name: data-analyzer
    build:
      context: data-analyzer
    volumes:
    - /var/logs/:/var/log/
    depends_on:
    - data-analyzer-storage
    - data-processor
    expose:
    - 8013
    ports:
    - 8013:8013
    environment:
    - SPRING_PROFILES_ACTIVE=${SPRING_PROFILE}
    - SPRING_DATA_MONGODB_DATABASE=analyzer
    - SPRING_DATA_MONGODB_URI=mongodb://data-analyzer:data-analyzer@data-analyzer-storage:27017/?uuidRepresentation=standard
    - SPRING_ACTIVEMQ_BROKER_URL=tcp://data-processing-bus:61616

  data-processor-storage:
    image: 'postgres:13.1-alpine'
    container_name: data-processor-storage
    expose:
    - 5432
    ports:
    - 5432:5432
    environment:
    - POSTGRES_USER=data-processor
    - POSTGRES_PASSWORD=data-processor
    - POSTGRES_DB=data-processor

  data-processing-bus:
    image: 'rmohr/activemq:5.15.9'
    container_name: data-processing-bus
    expose:
    - 61616
    - 8161
    ports:
    - 61616:61616
    - 8161:8161

  data-reciever-storage:
    image: 'minio/minio'
    container_name: data-reciever-storage
    volumes:
    - ~/data/minio-persistence:/data
    command: server /data
    expose:
    - 9000
    ports:
    - 9000:9000
    environment:
      MINIO_ROOT_USER: minioadmin
      MINIO_ROOT_PASSWORD: minioadmin

  data-analyzer-storage:
    image: 'mongo:4.4.5-bionic'
    container_name: data-analyzer-storage
    hostname: data-analyzer-storage
    expose:
    - 27017
    ports:
    - 27017:27017
    environment:
      MONGO_INITDB_ROOT_USERNAME: data-analyzer
      MONGO_INITDB_ROOT_PASSWORD: data-analyzer
      MONGO_INITDB_DATABASE: analyzer

  prometheus:
    image: prom/prometheus:v2.23.0
    container_name: monitoring-prometheus
    volumes:
    - ./monitoring/prometheus/prometheus.yml:/etc/prometheus/prometheus.yml
    command: "--config.file=/etc/prometheus/prometheus.yml --storage.tsdb.path=/prometheus"
    ports:
    - "9090:9090"
  promtail:
    image: grafana/promtail:2.1.0
    container_name: monitoring-promtail
    volumes:
    - ./monitoring/promtail/promtail-docker-config.yaml:/etc/promtail/docker-config.yaml
    - /var/logs/:/var/logs/
    command: "--config.file=/etc/promtail/docker-config.yaml"
  loki:
    image: grafana/loki:2.1.0
    container_name: monitoring-loki
    command: -config.file=/etc/loki/local-config.yaml
    ports:
    - "3100:3100"
  grafana:
    image: grafana/grafana:7.3.6
    container_name: monitoring-grafana
    volumes:
    - ./monitoring/grafana/grafana.ini:/etc/grafana/grafana.ini
    - ./monitoring/grafana/datasource.yaml:/etc/grafana/provisioning/datasources/datasource.yaml
    - ./monitoring/grafana/dashboard.yaml:/etc/grafana/provisioning/dashboards/dashboard.yaml
    - ./monitoring/grafana/example.json:/var/lib/grafana/dashboards/example.json
    ports:
    - "3000:3000"
    depends_on:
    - prometheus
    - loki
  jaeger:
    image: jaegertracing/all-in-one:1.21.0
    container_name: monitoring-jaeger
    command: "--collector.zipkin.http-port=9411 --log-level=info"
    ports:
    - "5775:5775/udp"
    - "6831:6831/udp"
    - "6832:6832/udp"
    - "5778:5778"
    - "16686:16686"
    - "14268:14268"
    - "14250:14250"
    - "9411:9411"

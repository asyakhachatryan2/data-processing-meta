<!-- TABLE OF CONTENTS -->

## Table of Contents

<details open="open">
   <ul>
      <li>
         <a href="#installing">Installing</a>
         <ul>
            <li><a href="#running-the-application-via-docker-container">Running the application via docker compose</a></li>
            <li><a href="#running-the-application-with-ide">Running the application with IDE</a></li>
            <li><a href="#running-the-application-with-maven">Running the application with Maven</a></li>
            <li>
               <a href="#running-the-application-with-executable-jar">Running the application with Executable JAR</a>
            </li>
         </ul>
      </li>
      <li>
         <a href="#testing-api">Testing API</a>
         <ul>
            <li><a href="#testing-with-maven">Testing with Maven</a></li>
            <li><a href="#testing-with-ide">Testing with IDE</a></li>
            <li><a href="#testing-with-swagger">Testing with Swagger</a></li>
            <li><a href="#testing-with-postman">Testing with Postman</a></li>
         </ul>
      </li>
   </ul>
</details>

## Installing

* Default active profile is **`dev`**. When the application is running, **Liquibase** will create the necessary tables
  and system data along with sample data.

* Other sample profiles like **`prod`** are available. Change the **spring.profiles.active** property in the
  **application.yml** file to any of the aforementioned profiles to use it.

* Give permissions to the `/var/logs/` folder.

```shell
$ cd /var/
$ sudo mkdir logs
$ sudo chmod 777 logs
```

#### Running the application via docker compose

Check the **docker-compose.yml** file

**NOTE:** If you are facing any issues with accessing the application please make sure your docker has enough resources
and all the ports described in docker-compose.yml are free.
**NOTE:** Here is assumption that you're already cloned all the necessary repositories from GitLab. Please refer to
README.md for instructions.

|                  Command          |                                             Description                                     |
|-----------------------------------|---------------------------------------------------------------------------------------------| 
|`docker-compose config`            | Check the build-file for syntax-errors                                                      |
|`docker-compose up`                | Start the containers                                                                        |
|`docker-compose down`                | Stop the containers, remove them from Docker and remove the connected networks from it.     |

```shell
$ cd data-processing-meta
$ docker-compose up
```

#### Running the application with IDE

**NOTE:** Make sure you run docker-compose.yaml to have all the desired infrastructure. Read here for
instructions [Running the application via docker compose](#running-the-application-via-docker-container). Before running
the application from IDE stop the related container from the docker.

```shell
$ docker stop {conatiner-name}
```

There are several ways to run a Spring Boot application on your local machine. One way is to execute the `main` method
from your IDE.

* Import all mavens project to IntelliJ.
* Right-Click on the file *Application.java from the main package and click `Run '*Application'`.

If you need to run against `prod` profile, in the ```Run/Debug Configuration``` chose your application and add in
the ```JVM Options``` `-Dspring.profiles.active=prod` line.

#### Running the application with Maven

Alternatively you can use
the [Spring Boot Maven plugin](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html)
like so:

**NOTE:** Make sure you run docker-compose.yaml to have all the desired infrastructure. Read here for
instructions [Running the application via docker compose](#running-the-application-via-docker-container). Before running
the application from IDE stop the appropriate container from docker.

```shell
$ docker stop {conatiner-name}
```

```shell
$ cd cd data-processing-meta/data-{application}
$ mvn spring-boot:run
```

#### Running the application with Executable JAR

The code can also be built into a jar and then executed/run. Once the jar is built, run the jar by double clicking on it
or by using the command

```shell
$ cd cd data-processing-meta/data-{application}
$ mvn clean package -DskipTests
$ java -jar target/data-{application}-0.0.1-SNAPSHOT.jar --spring.profiles.active=dev
```

In the shell script above change the `{application}` with target application i.e receiver, processor or analyzer

If you need to run against `prod` profile, change `--spring.profiles.active=` to `prod`

## Testing API

**NOTE:** Please make sure all the components of the application are running.

Every time when you running `Data Simulator` make sure you put different date
for `simulator.generate-for-date-as-string` property in `data-simulator\application.yml` file. Please refer
to `data-simulator\application.yml` file to configure different properties of the load tests.

### Testing with Maven

* Run only unit tests:

```shell
$ mvn clean test
```

* Run Data Simulator:

Ever `simulator.generate-for-date-as-string` property in `data-simulator\application.yml` file.

```shell
$ cd data-processing-meta/data-simulator
$ mvn spring-boot:run
```

To see the `output` about actions performed run the following:

```shell
$ cd log
$ cat data-simulator.log
```

### Testing with IDE

* Import data-simulator mavens project to IntelliJ.
* Right Click on the file SimulatorApplication.java from the main package and click `Run 'SimulatorApplication'`.

### Testing with Swagger

* Hit [Data Receiver](http://localhost:8011/docs/swagger-ui) for the documentation how to generate a simple click.
* Hit [Data Analyzer](http://localhost:8013/docs/swagger-ui) for the documentation how to get statistics data.

### Testing with Postman

* Hit [Data Receiver](https://documenter.getpostman.com/view/17239421/UVR4NVBD) for the documentation how to generate a simple click.
* Hit [Data Analyzer](https://documenter.getpostman.com/view/17239421/UVR4NVB7) for the documentation how to get statistics data.

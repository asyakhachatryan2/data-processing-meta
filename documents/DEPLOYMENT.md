# Deployment

# Deployment on AWS EC2 instance

1. Create **Ubuntu Server 18.04 LTS (HVM)** EC2 instance on AWS

2. Do SSH on that instance from shell

```shell
ssh -i "data-processing.pem" ubuntu@ec2-3-236-188-42.compute-1.amazonaws.com
# the pem file for security purposes is not provided it should be located in the current directory
```

3. Install [Docker on EC2](https://docs.docker.com/engine/install/ubuntu/)

4. Make necessary `Prerequisites` step from [README.md](../README.md)

5. Set environment variable `SPRING_PROFILE` equal to `prod` to run the `PLatform` against AWS compnents.

```shell
export export SPRING_PROFILE=prod
```

6. Create directory for logs

```shell
$ cd /var/
$ sudo mkdir logs
$ sudo chmod 777 logs
```

7. Run `docker-compose` to deploy platform to the `docker`.
   **Note:** for details please refer [RUNBOOK.md](RUNBOOK.md#installing)

```shell
$ docker-compose up
```

8. Configure *AWS VPC* to allow `Inboud/Outbound` requests.

## Cloudformation

`TODO:` Here should go the instructions for `Cloudformation`

## CI/CD

`TODO:`
After commit on `master` branch, the `GitLab` pipeline should run test `mvn clean verify checkstyle:check`, build the
image and deploy to the `AWS EKS`

